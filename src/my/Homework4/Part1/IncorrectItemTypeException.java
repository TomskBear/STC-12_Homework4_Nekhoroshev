package my.Homework4.Part1;

public class IncorrectItemTypeException extends Exception {

    private static final String message = "You can not add not Number types in this ObjectBox";

    public IncorrectItemTypeException() {
        super(message);
    }
}
