package my.Homework4.Part1;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ObjectBox {
    private List<Object> internalList;

    public ObjectBox() {
        this.internalList = new ArrayList<>();
    }

    public void addObject(Object newObject) {
        if (newObject != null) {
            internalList.add(newObject);
        }
    }

    public void deleteObject(Object objectForRemove) {
        if (objectForRemove != null && internalList.contains(objectForRemove)) {
            internalList.remove(objectForRemove);
        }
    }

    private void checkIfItemIsHasCorrectType(Object item) throws IncorrectItemTypeException {
        if (!Number.class.isAssignableFrom(item.getClass())) {
            throw new IncorrectItemTypeException();
        }
    }

    public Double summator() throws IncorrectItemTypeException {
        Double result = 0.0;
        for (Object listItem : internalList) {
            checkIfItemIsHasCorrectType(listItem);
            result += Double.valueOf(listItem.toString());
        }
        return result;
    }

    public List<Double> splitter(Double divider) throws IncorrectItemTypeException {
        List<Double> result = new ArrayList<>();
        for (Object listItem : internalList) {
            checkIfItemIsHasCorrectType(listItem);
            result.add(Double.valueOf(listItem.toString()) / divider);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectBox objectBox = (ObjectBox) o;
        return Objects.equals(internalList, objectBox.internalList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(internalList);
    }

    public String dump() {
        return "ObjectBox{" +
                "internalList=" + internalList +
                '}';
    }
}
