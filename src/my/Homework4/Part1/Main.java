package my.Homework4.Part1;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        ObjectBox box = new ObjectBox();
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);

        try {
            Double result = box.summator();
            System.out.println("Summa = " + result);
        } catch (IncorrectItemTypeException ex) {
            ex.printStackTrace();
        }

        try {
            List<Double> result = box.splitter(2.0);
            System.out.println("Splitted = " + result);
        } catch (IncorrectItemTypeException ex) {
            ex.printStackTrace();
        }

        System.out.println(box.dump());
    }
}
