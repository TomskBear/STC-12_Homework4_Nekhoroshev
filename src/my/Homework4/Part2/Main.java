package my.Homework4.Part2;

import my.Homework4.Part1.IncorrectItemTypeException;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        MathBox box = new MathBox();
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);
        box.addObject(1);

        try {
            Double result = box.summator();
            System.out.println("Summa = " + result);
        } catch (IncorrectItemTypeException ex) {
            ex.printStackTrace();
        }

        try {
            List<Double> result = box.splitter(2.0);
            System.out.println("Split = " + result);
        } catch (IncorrectItemTypeException ex) {
            ex.printStackTrace();
        }

        System.out.println(box.dump());
    }
}
