package my.Homework4.Part2;

import my.Homework4.Part1.IncorrectItemTypeException;

import java.util.ArrayList;
import java.util.List;

public class MathBox extends ObjectBox {
    public Double summator() throws IncorrectItemTypeException {
        Double result = 0.0;
        for (Object listItem : internalList) {
            checkIfItemIsHasCorrectType(listItem);
            result += Double.valueOf(listItem.toString());
        }
        return result;
    }

    public List<Double> splitter(Double divider) throws IncorrectItemTypeException {
        List<Double> result = new ArrayList<>();
        for (Object listItem : internalList) {
            checkIfItemIsHasCorrectType(listItem);
            result.add(Double.valueOf(listItem.toString()) / divider);
        }
        return result;
    }
}
