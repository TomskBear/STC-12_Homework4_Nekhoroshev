package my.Homework4.Part2;

import my.Homework4.Part1.IncorrectItemTypeException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ObjectBox {
    protected List<Object> internalList;

    public ObjectBox() {
        this.internalList = new ArrayList<>();
    }

    public void addObject(Object newObject) {
        if (newObject != null) {
            internalList.add(newObject);
        }
    }

    public void deleteObject(Object objectForRemove) {
        if (objectForRemove != null && internalList.contains(objectForRemove)) {
            internalList.remove(objectForRemove);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectBox objectBox = (ObjectBox) o;
        return Objects.equals(internalList, objectBox.internalList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(internalList);
    }

    public String dump() {
        return "ObjectBox{" +
                "internalList=" + internalList +
                '}';
    }

    protected void checkIfItemIsHasCorrectType(Object item) throws IncorrectItemTypeException {
        if (!Number.class.isAssignableFrom(item.getClass())) {
            throw new IncorrectItemTypeException();
        }
    }
}
